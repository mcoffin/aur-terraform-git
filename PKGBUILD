# Maintainer: Matt Coffin <mcoffin13@gmail.com>
pkgname=terraform-git
pkgver=v0.13.5.r0.44aeaa59e7
pkgrel=1
pkgdesc="HashiCorp tool for building and updating infrastructure as code idempotently (git version)"
arch=('x86_64')
url="https://terraform.io"
license=('GPL')
groups=()
depends=()
makedepends=(git go) # 'bzr', 'git', 'mercurial' or 'subversion'
provides=("${pkgname%-git}")
conflicts=("${pkgname%-git}")
replaces=()
backup=()
options=()
install=
source=("${pkgname%-git}::git+git://github.com/hashicorp/terraform.git?signed#tag=v0.13.5"
        "25555.patch")
noextract=()
sha512sums=('SKIP'
            'bb88abb0b57d2a1f779df2036fe529cb17c95fbfc8831a79fdb7a97a7790d3e7247998c2757d7dd7c6193dd48bc99893363363dcf2b26f6d1ecc6f4cb2568c2a')
validpgpkeys=(
	# See: https://www.hashicorp.com/security, https://terraform.io/downloads.html
	# HashiCorp Security <security@hashicorp.com>
	'91A6E7F85D05C65630BEF18951852D87348FFC4C'
)

pkgver() {
	cd "$srcdir/${pkgname%-git}"

	# Git, tags available
	printf "%s" "$(git describe --long | sed 's/\([^-]*-\)g/r\1/;s/-/./g')"

	# Git, no tags available
	# printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
	cd "$srcdir/${pkgname%-git}"
	patch -p1 < "$srcdir"/25555.patch
}

build() {
	cd "$srcdir/${pkgname%-git}"
	export CGO_CPPFLAGS="$CPPFLAGS"
	export CGO_CFLAGS="$CFLAGS"
	export CGO_CXXFLAGS="$CXXFLAGS"
	export CGO_LDFLAGS="$LDFLAGS"
	# From upstream
	# TODO: maybe we should check if -fPIE is present in standard flags?
	export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw -ldflags=-linkmode=external"
	go build -o terraform-binary
}

check() {
	cd "$srcdir/${pkgname%-git}"
	go test -mod=vendor ./...
}

package() {
	cd "$srcdir/${pkgname%-git}"
	install -Dm755 terraform-binary "$pkgdir"/usr/bin/terraform
	local license_file
	for license_file in LICENSE COPYING; do
		[ -f "$license_file" ] || continue
		install -Dm 0644 -t "$pkgdir"/usr/share/licenses/"$pkgname" "$license_file"
	done
}
